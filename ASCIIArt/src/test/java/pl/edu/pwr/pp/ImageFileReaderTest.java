package pl.edu.pwr.pp;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ImageFileReaderTest {
	
	ImageFileReader imageReader;
	
	@Before
	public void setUp() {
		imageReader = new ImageFileReader();
	}

	@Test
	public void shouldReadSequenceFrom0To255GivenTestImage() {
		// given
		//String fileName = "C:/Users/Student/git/asciiartv3/ASCIIArt/src/test/resources/testImage.pgm";
		String file= "testImage.pgm";
		Path path=null;
		try {
			path = this.getPathToFile(file);
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// when
		int[][] intensities = null;
		try {
			intensities = imageReader.readPgmFile(path.toString());
		} catch (URISyntaxException e) {
			Assert.fail("Should read the file");
		}
		// then
		int counter = 0;
		for (int[] row : intensities) {
			for (int intensity : row) {
				assertThat(intensity, is(equalTo(counter++)));
			}
		}
	}
	private Path getPathToFile(String fileName) throws URISyntaxException {
 		URI uri = ClassLoader.getSystemResource(fileName).toURI();
 		return Paths.get(uri);
 	}
	
	@Test
	public void shouldThrowExceptionWhenFileDontExist() {
		// given
		//String fileName = "C:/Users/Student/git/asciiartv3/ASCIIArt/src/test/resources/nonexistent.pgm";
		String file= "nonexistent.pgm";

		try {
			Path path = this.getPathToFile(file);
			imageReader.readPgmFile(path.toString());
			// then
			Assert.fail("Should throw exception");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			assertThat(e, is(instanceOf(NullPointerException.class)));
		}

		
	}
	 
	@Test
	public void shouldNotBeNull(){
		try {
			URL url = new URL("http://agata.migalska.staff.iiar.pwr.wroc.pl/PP2016/obrazy/eiffeltower.pgm");
			int [][] result =imageReader.readPgmUrl(url);
			Assert.assertThat(result,Matchers.not(Matchers.equalTo(null)));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
