package pl.edu.pwr.pp;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class ImageToGrayTest {

	@Mock private BufferedImage image;
	int [][] picture;


	@Before
	public void setUp() {
	MockitoAnnotations.initMocks(this);
	picture = new int[1][1];
	picture[0][0]=195;
	}
	
	@Test
	public void shouldRescaleImageWithPreservedRatio() {
	Mockito.when(image.getWidth()).thenReturn(300);
	Mockito.when(image.getHeight()).thenReturn(150);
	BufferedImage result = ImageConverter.resizeImage(image, BufferedImage.TYPE_BYTE_GRAY, 100);
	Assert.assertThat(result.getHeight(), Matchers.is(Matchers.equalTo(50)));
	Mockito.verify(image).getWidth();
	Mockito.verify(image).getHeight();
	}
	
	@Test
	public void shouldRgbImageConvertToGray(){

		String file= "testGray.png";
		Path path=null;
		try {
			path = this.getPathToFile(file);
			image = ImageIO.read(new File(path.toString()));
			int [][] intensities=null;
			intensities=ImageConverter.ConvertToGray(image,false);
			Assert.assertThat(intensities[1][1], Matchers.is(Matchers.equalTo(201)));
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void shouldGrayDoNotImageConvertToGray(){

		String file= "testGray2.png";
		Path path=null;
		try {
			path = this.getPathToFile(file);
			image = ImageIO.read(new File(path.toString()));
			int [][] intensities=null;
			intensities=ImageConverter.ConvertToGray(image,true);
			Assert.assertThat(intensities[1][1], Matchers.is(Matchers.equalTo(195)));
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private Path getPathToFile(String fileName) throws URISyntaxException {
 		URI uri = ClassLoader.getSystemResource(fileName).toURI();
 		return Paths.get(uri);
 	}
	
	@Test
	public void shouldReturnIntensitiesToAscii(){
		char[][] asciiArt= ImageConverter.intensitiesToAscii(picture);
		char znakAscii=':';
		Assert.assertThat(asciiArt[0][0], Matchers.is(Matchers.equalTo(znakAscii)));
	}
	
	@Test
	public void shouldBufferedImageConvertToImageWithTheRightWidth(){
		BufferedImage image = ImageConverter.ConvertToImage(picture, 1, 1);
		Assert.assertThat(image.getWidth(), Matchers.is(Matchers.equalTo(1)));
	}
}
