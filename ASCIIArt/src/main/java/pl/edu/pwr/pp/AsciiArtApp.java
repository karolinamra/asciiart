package pl.edu.pwr.pp;

import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import java.awt.Font;
import javax.swing.JComboBox;

public class AsciiArtApp extends JFrame implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JFrame frame;
	private JLabel lblImage;
	private JComboBox<String> comboBox_Quality;
	private FormWczytaj formWczytaj;
	private ImageFileReader readPgm;
	private JButton btnZapiszObraz;
	private int[][] picture;
	private String[] optionQuality ={"Niska", "Wysoka"};
	private String[] optionWidth ={"80 znaków", "160 znaków", "Szerokość ekranu", "Oryginalna"};
	private JLabel lblSzeroko;
	private JComboBox<String> comboBox_Width;
	private BufferedImage imageToAscii;
	boolean isGray;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AsciiArtApp window = new AsciiArtApp();
					window.frame.setVisible(true);
					 
					 
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AsciiArtApp() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(192, 192, 192));
		frame.setBounds(100, 100, 786, 461);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("Wczytaj obraz");
		btnNewButton.addActionListener(this);
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setBackground(new Color(255, 0, 51));
		btnNewButton.setBounds(10, 46, 205, 34);
		frame.getContentPane().add(btnNewButton);
		
		
		btnZapiszObraz = new JButton("Zapisz do pliku");
		btnZapiszObraz.addActionListener(
				e->{
					int newWidth=HowWidth();
					JFileChooser fileChooser = new JFileChooser();
					int retrival = fileChooser.showSaveDialog(null);
				    if (retrival == JFileChooser.APPROVE_OPTION) {
				    	
				    	if(newWidth==-1) newWidth=imageToAscii.getWidth();
				    	HowQuality();
				        ImageFileWriter writer = new ImageFileWriter();
				        BufferedImage newImage = new BufferedImage(imageToAscii.getWidth(),imageToAscii.getHeight(),
				        		BufferedImage.TYPE_BYTE_GRAY);
				        newImage=ImageConverter.resizeImage(imageToAscii, BufferedImage.TYPE_BYTE_GRAY, newWidth);
				        picture=ImageConverter.ConvertToGray(newImage, isGray);
						//picture=ImageConverter.ConvertToGray(imageToAscii);
				        writer.saveToTxtFile(ImageConverter.intensitiesToAscii(picture), fileChooser.getSelectedFile()+"");
				    }
				});
		btnZapiszObraz.setEnabled(false);
		btnZapiszObraz.setForeground(Color.WHITE);
		btnZapiszObraz.setBackground(new Color(255, 0, 51));
		btnZapiszObraz.setBounds(10, 198, 205, 34);
		frame.getContentPane().add(btnZapiszObraz);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		panel.setBounds(225, 11, 535, 400);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		lblImage = new JLabel("Miejsce na obrazek");
		//lblImage.setIcon(new ImageIcon(AsciiArtApp.class.getResource("/com/sun/javafx/scene/control/skin/modena/HTMLEditor-Background-Color-Black.png")));
		lblImage.setVerticalAlignment(SwingConstants.BOTTOM);
		lblImage.setHorizontalAlignment(SwingConstants.CENTER);
		lblImage.setBounds(10, 11, 515, 378);
		panel.add(lblImage);
		
		JLabel lblJako = new JLabel("Jakość:");
		lblJako.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblJako.setForeground(new Color(255, 0, 51));
		lblJako.setBounds(10, 103, 56, 24);
		frame.getContentPane().add(lblJako);
		
		comboBox_Quality = new JComboBox<String>(optionQuality);
		comboBox_Quality.setSelectedIndex(0);
		comboBox_Quality.setBounds(66, 106, 149, 20);
		frame.getContentPane().add(comboBox_Quality);
		
		lblSzeroko = new JLabel("Szerokość:");
		lblSzeroko.setForeground(new Color(255, 0, 51));
		lblSzeroko.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSzeroko.setBounds(10, 144, 71, 24);
		frame.getContentPane().add(lblSzeroko);
		
		comboBox_Width = new JComboBox<String>(optionWidth);
		comboBox_Width.setSelectedIndex(0);
		comboBox_Width.setBounds(91, 147, 124, 20);
		frame.getContentPane().add(comboBox_Width);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		// TODO Auto-generated method stub
		if(formWczytaj==null) formWczytaj = new FormWczytaj(this);
		formWczytaj.setVisible(true);
		if(formWczytaj.isOk())
		{
			readPgm = new ImageFileReader();
			picture=null;
			if(formWczytaj.opcja=="plik"){
				try {
					picture = readPgm.readPgmFile(formWczytaj.getData());
					if(readPgm.isPgm)
					{
						isGray=true;
						btnZapiszObraz.setEnabled(true);
						imageToAscii = ImageConverter.ConvertToImage(picture, readPgm.width, readPgm.height);
						lblImage.setIcon(new ImageIcon(imageToAscii.getScaledInstance(lblImage.getWidth(), lblImage.getHeight(), Image.SCALE_SMOOTH)));
					}
					else
					{
						isGray=false;
						btnZapiszObraz.setEnabled(true);
						try {
							imageToAscii = ImageIO.read(new File(formWczytaj.getData()));							
							lblImage.setIcon(new ImageIcon(imageToAscii.getScaledInstance(lblImage.getWidth(), lblImage.getHeight(),
					        Image.SCALE_SMOOTH)));
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
					}
					
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else{
				btnZapiszObraz.setEnabled(true);
				try {
					if(formWczytaj.getData().contains("pgm")){
						isGray=true;
						picture = readPgm.readPgmUrl(new URL(formWczytaj.getData()));
						imageToAscii = ImageConverter.ConvertToImage(picture, readPgm.width, readPgm.height);
						lblImage.setIcon(new ImageIcon(imageToAscii.getScaledInstance(lblImage.getWidth(), lblImage.getHeight(), Image.SCALE_SMOOTH)));
					}	
					else{
						isGray=false;
						try {
							imageToAscii = ImageIO.read(new URL(formWczytaj.getData()));
							lblImage.setIcon(new ImageIcon(imageToAscii.getScaledInstance(lblImage.getWidth(), lblImage.getHeight(),Image.SCALE_SMOOTH)));
						} catch (IOException e1) {
							// TODO Auto-generated catch block
					        JOptionPane.showMessageDialog(null, "Błędy adres URL", "Warning ", JOptionPane.WARNING_MESSAGE);
						}
						
					}
					
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
			
		}
		
	}

	private void HowQuality(){
		int opcja = comboBox_Quality.getSelectedIndex();
		switch(opcja){
		case 0: ImageConverter.INTENSITY_2_ASCII="@%#*+=-:. ";
			break;
		case 1: ImageConverter.INTENSITY_2_ASCII = "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\\|()1{}[]?-_+~<>i!lI;:,\"^`'. ";
			break;
			default: ImageConverter.INTENSITY_2_ASCII="@%#*+=-:. ";
		}
	}
	
	private int HowWidth(){
		int opcja=comboBox_Width.getSelectedIndex();
		switch(opcja){
		case 0: return 80;
		case 1:  return 160;
		case 2:
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			return (int) screenSize.getWidth(); 
		case 3: return -1;
		}
		return -1;
	}


	
	
}
