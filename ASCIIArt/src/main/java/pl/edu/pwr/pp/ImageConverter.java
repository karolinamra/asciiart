package pl.edu.pwr.pp;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

public class ImageConverter {

	/**
	 * Znaki odpowiadające kolejnym poziomom odcieni szarości - od czarnego (0)
	 * do białego (255).
	 */
	public static String INTENSITY_2_ASCII="@%#*+=-:. ";

	/**
	 * Metoda zwraca znak odpowiadający danemu odcieniowi szarości. Odcienie
	 * szarości mogą przyjmować wartości z zakresu [0,255]. Zakres jest dzielony
	 * na równe przedziały, liczba przedziałów jest równa ilości znaków w
	 * {@value #INTENSITY_2_ASCII}. Zwracany znak jest znakiem dla przedziału,
	 * do którego należy zadany odcień szarości.
	 * 
	 * 
	 * @param intensity
	 *            odcień szarości w zakresie od 0 do 255
	 * @return znak odpowiadający zadanemu odcieniowi szarości
	 */
	public static char intensityToAscii(int intensity) {
		int index=intensity*INTENSITY_2_ASCII.length()/256;
		return INTENSITY_2_ASCII.charAt(index);
	}

	/**
	 * Metoda zwraca dwuwymiarową tablicę znaków ASCII mając dwuwymiarową
	 * tablicę odcieni szarości. Metoda iteruje po elementach tablicy odcieni
	 * szarości i dla każdego elementu wywołuje {@ref #intensityToAscii(int)}.
	 * 
	 * @param intensities
	 *            tablica odcieni szarości obrazu
	 * @return tablica znaków ASCII
	 */
	public static char[][] intensitiesToAscii(int[][] intensities) {
		char[][] asciiArt = null;
		// inicjalizacja tablicy
		asciiArt = new char[intensities.length][];

		for (int i = 0; i < intensities.length; i++) {
			asciiArt[i] = new char[intensities[i].length];
		}
	

		
		for(int i=0; i< intensities.length; i++){
			for(int j=0; j<intensities[i].length; j++){
				asciiArt[i][j]=intensityToAscii(intensities[i][j]);
			}
		}
		
		return asciiArt;
	}
	
	public static BufferedImage ConvertToImage(int[][] picture, int w, int h) {
		BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_BYTE_GRAY);
		WritableRaster raster = image.getRaster();
		for (int y = 0; y < h; y++) {
		  for (int x = 0; (x < w); x++) {
		    raster.setSample(x, y, 0, picture[y][x]);
		  }
		}
		return image;
	}
	
	public static int [][] ConvertToGray(BufferedImage image, boolean isGray){
		int [][] intensities=null;
		// inicjalizacja tablicy na odcienie szarości
		intensities = new int[image.getHeight()][];
		for (int i = 0; i < image.getHeight(); i++) {
			intensities[i] = new int[image.getWidth()];
		}
		
		for(int i=0; i<image.getHeight(); i++){
			for(int j=0; j<image.getWidth(); j++){
				Color color= new Color(image.getRGB(j, i));
				if(isGray) intensities[i][j]=color.getRed();
				else intensities[i][j]=(int)(0.2989*color.getRed()+0.5870*color.getGreen()+0.1140*color.getBlue());
				}
		}
			
		return intensities;
	}
	 
	public static BufferedImage resizeImage(BufferedImage originalImage, int type, int w) {
		 int h=w*originalImage.getHeight()/originalImage.getWidth();
		 BufferedImage resizedImage = new BufferedImage(w,h,type);
		 Graphics2D g = resizedImage.createGraphics();
		 g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
		 RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		 g.drawImage(originalImage, 0, 0, w, h, null);
		 g.dispose();
		 return resizedImage;
		}

}
