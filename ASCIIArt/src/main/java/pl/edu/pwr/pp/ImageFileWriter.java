package pl.edu.pwr.pp;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.stream.IntStream;

public class ImageFileWriter {

	public void saveToTxtFile(char[][] ascii, String fileName) {
		// np. korzystając z java.io.PrintWriter
		PrintWriter saveFile;
		try {
			saveFile = new PrintWriter(fileName);
	
			IntStream.range(0,ascii.length).forEach(
					i-> saveFile.println(ascii[i]));
				saveFile.close();
				
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
}
