package pl.edu.pwr.pp;

import javax.swing.JFrame;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;

public class FormWczytaj extends JDialog implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JFrame frame;
	private JTextField textField_Url;
	private JTextField textField_Plik;
	private String path;
	public String opcja;
	private ButtonGroup buttonGroup;
	public boolean okData;
	private JButton btnWczytaj;

	
	/**
	 * Create the application.
	 */
	public FormWczytaj(JFrame owner) {
		super(owner, "Wczytywanie z pliku", true);
		getContentPane().setBackground(new Color(192, 192, 192));
		initialize();
	}

	public String getData(){
		return path;
	}
	
	public boolean isOk()
	{
		return okData;
	}
	
	private String getSelectedButtonText(ButtonGroup buttonGroup) {
        for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();

            if (button.isSelected()) {
                return button.getText();
            }
        }

        return null;
    }
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		path=null;
		opcja=null;
		//frame = new JFrame();
		setBounds(100, 100, 546, 242);
		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JRadioButton rdbtnWczytajUrl = new JRadioButton("z URL");
		rdbtnWczytajUrl.setFont(new Font("Tahoma", Font.PLAIN, 12));
		rdbtnWczytajUrl.setBackground(new Color(192, 192, 192));
		rdbtnWczytajUrl.setBounds(6, 73, 80, 23);
		getContentPane().add(rdbtnWczytajUrl);
		
		JRadioButton rdbtnWczytajPlik = new JRadioButton("z Pliku");
		rdbtnWczytajPlik.setFont(new Font("Tahoma", Font.PLAIN, 12));
		rdbtnWczytajPlik.setBackground(new Color(192, 192, 192));
		rdbtnWczytajPlik.setBounds(6, 101, 80, 23);
		rdbtnWczytajPlik.setSelected(true);
		getContentPane().add(rdbtnWczytajPlik);
		
		buttonGroup =new ButtonGroup();
		buttonGroup.add(rdbtnWczytajPlik);
		buttonGroup.add(rdbtnWczytajUrl);
		
		textField_Url = new JTextField();
		textField_Url.setBounds(92, 74, 406, 20);
		getContentPane().add(textField_Url);
		textField_Url.setColumns(10);
		
		textField_Plik = new JTextField();
		textField_Plik.setEditable(false);
		textField_Plik.setBounds(92, 102, 309, 20);
		getContentPane().add(textField_Plik);
		textField_Plik.setColumns(10);
		
		btnWczytaj = new JButton("Wczytaj");
		btnWczytaj.setForeground(new Color(255, 255, 255));
		btnWczytaj.setBackground(new Color(255, 0, 51));
		btnWczytaj.addActionListener(this);
		btnWczytaj.setBounds(92, 158, 103, 23);
		getContentPane().add(btnWczytaj);
		
		JButton btnAnuluj = new JButton("Anuluj");
		btnAnuluj.setForeground(new Color(255, 255, 255));
		btnAnuluj.setBackground(new Color(255, 0, 51));
		btnAnuluj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnAnuluj.setBounds(298, 158, 103, 23);
		getContentPane().add(btnAnuluj);
		
		JButton btnPrzegladaj = new JButton("Przegladaj");
		btnPrzegladaj.setForeground(new Color(255, 255, 255));
		btnPrzegladaj.setBackground(new Color(255, 0, 51));
		btnPrzegladaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
				int returnVal = chooser.showOpenDialog(frame);
			    if(returnVal == JFileChooser.APPROVE_OPTION) {
			       textField_Plik.setText(chooser.getSelectedFile().getAbsolutePath());
			    	
			    	//System.out.println("You chose to open this file: " +			            chooser.getSelectedFile().getName());
			    }
			}
		});
		btnPrzegladaj.setBounds(409, 101, 89, 23);
		getContentPane().add(btnPrzegladaj);
		
		JLabel lblWczytajZ = new JLabel("Wczytaj z...");
		lblWczytajZ.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblWczytajZ.setBounds(155, 18, 103, 45);
		getContentPane().add(lblWczytajZ);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Object z = e.getSource();
		if(z==btnWczytaj)
		{
		okData=true;
		if(getSelectedButtonText(buttonGroup)=="z Pliku"){
				path=textField_Plik.getText();
				opcja="plik";
		}
		else{
				path=textField_Url.getText();
				opcja="url";
			}
		}
		else okData=false;

		this.setVisible(false);
		
	}
}
